<?php

namespace Drupal\jsonld_simple\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Checks that the submitted value is in json format.
 *
 * @package Drupal\jsonld_simple\Plugin\Validation\Constraint
 */
class IsJsonFormatValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($item, Constraint $constraint) {
    if (isset($item->value) && ($item->value != "")) {
      // Validate if the data is valid json format.
      $result = json_decode($item->value);
      if (json_last_error() !== JSON_ERROR_NONE) {
        // JSON is not valid.
        $this->context->addViolation($constraint->message);
      }
    }
  }

}
