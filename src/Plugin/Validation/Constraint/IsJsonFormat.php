<?php

namespace Drupal\jsonld_simple\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the submitted value is in json format.
 *
 * @Constraint(
 *   id = "IsJsonFormat",
 *   label = @Translation("Is the data in JSON format", context = "Validation"),
 *   type = "string"
 * )
 */
class IsJsonFormat extends Constraint {

  /**
   * The message that will shown if the value is empty.
   *
   * @var string
   */
  public $message = 'The data should be in JSON format.';

}
