<?php

namespace Drupal\jsonld_simple\Form;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field\Entity\FieldConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Configure jsonld_simple settings for this site.
 */
class JsonldSimpleSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'jsonld_simple.settings';

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * Logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs an AutoParagraphForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   Entity display repository service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   Logger factory service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, EntityDisplayRepositoryInterface $entity_display_repository, LoggerChannelFactoryInterface $loggerFactory) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityDisplayRepository = $entity_display_repository;
    $this->logger = $loggerFactory->get('jsonld_simple');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository'),
      $container->get('logger.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'jsonld_simple_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    try {
      $existingContentTypeOptions = $this->getJsonldContentTypes();
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      $this->logger->emergency('Error while fetching entities. ' . $e->getMessage() . $e->getTraceAsString());
    }

    $form['node_types'] = [
      '#type' => 'checkboxes',
      '#options' => $existingContentTypeOptions,
      '#default_value' => $this->config(static::SETTINGS)->get('node_types') ?: [],
      '#title' => $this->t('Enable JSON-LD for the node types'),
    ];

    $form['breadcrumb_structured_data_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable structured data for breadcrumbs'),
      '#default_value' => $this->config(static::SETTINGS)->get('breadcrumb_structured_data_enabled'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Returns a list of all the content types currently installed.
   *
   * @return array
   *   An array of content types.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getJsonldContentTypes() : array {
    $types = [];
    $contentTypes = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    foreach ($contentTypes as $contentType) {
      $types[$contentType->id()] = $contentType->label();
    }
    return $types;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('node_types', $form_state->getValue('node_types'))
      ->set('breadcrumb_structured_data_enabled', $form_state->getValue('breadcrumb_structured_data_enabled'))
      ->save();

    $this->addBodyField();
    $this->addBodyToggleField();
    parent::submitForm($form, $form_state);
  }

  /**
   * Add the  the JSON-LD body field.
   *
   */
  private function addBodyField() {
    // Create the field if not already exists.
    $field_storage = FieldStorageConfig::loadByName('node', 'field_jsonld_simple_body');
    if (empty($field_storage)) {
      $field_storage = FieldStorageConfig::create([
        'entity_type' => 'node',
        'type' => 'string_long',
        'field_name' => 'field_jsonld_simple_body',
      ]);
      $field_storage->save();
    }

    // Attach to the node types if not already done.
    $node_types = $this->config(static::SETTINGS)->get('node_types');
    foreach ($node_types as $bundle) {
      if ($bundle) {
        $field = FieldConfig::loadByName('node', $bundle, 'field_jsonld_simple_body');
        if (empty($field)) {
          $field = FieldConfig::create([
            'entity_type' => 'node',
            'field_name' => 'field_jsonld_simple_body',
            'label' => 'JSON-LD body',
            'field_storage' => $field_storage,
            'bundle' => $bundle,
            'settings' => ['display_summary' => FALSE],
          ]);
          $field->save();

          // Assign widget settings for the 'default' form mode.
          $this->entityDisplayRepository->getFormDisplay('node', $bundle, 'default')
            ->setComponent('field_jsonld_simple_body', [
              'type' => 'string_textarea',
              'weight' => 2,
              'settings' => ['rows' => '20'],
            ])
            ->save();

          // Assign display settings for the 'default' and 'teaser' view modes.
          $this->entityDisplayRepository->getViewDisplay('node', $bundle, 'default')
            ->removeComponent('field_jsonld_simple_body')
            ->save();
        }
      }
    }
  }

  /**
   * Add the  the JSON-LD body toggle field.
   *
   */
  private function addBodyToggleField() {
    // Create the field if not already exists.
    $field_storage = FieldStorageConfig::loadByName('node', 'field_jsonld_simple_body_toggle');
    if (empty($field_storage)) {
      $field_storage = FieldStorageConfig::create([
        'entity_type' => 'node',
        'type' => 'boolean',
        'field_name' => 'field_jsonld_simple_body_toggle',
      ]);
      $field_storage->save();
    }

    // Attach to the node types if not already done.
    $node_types = $this->config(static::SETTINGS)->get('node_types');
    foreach ($node_types as $bundle) {
      if ($bundle) {
        $field = FieldConfig::loadByName('node', $bundle, 'field_jsonld_simple_body_toggle');
        if (empty($field)) {
          $field = FieldConfig::create([
            'entity_type' => 'node',
            'field_name' => 'field_jsonld_simple_body_toggle',
            'label' => 'Disable JSON-LD output',
            'field_storage' => $field_storage,
            'bundle' => $bundle,
            'settings' => ['display_summary' => FALSE],
          ]);
          $field->save();

          // Assign widget settings for the 'default' form mode.
          $this->entityDisplayRepository->getFormDisplay('node', $bundle, 'default')
            ->setComponent('field_jsonld_simple_body_toggle', [
              'type' => 'boolean_checkbox',
              'weight' => 3,
            ])
            ->save();

          // Assign display settings for the 'default' and 'teaser' view modes.
          $this->entityDisplayRepository->getViewDisplay('node', $bundle, 'default')
            ->removeComponent('field_jsonld_simple_body_toggle')
            ->save();
        }
      }
    }
  }

}
